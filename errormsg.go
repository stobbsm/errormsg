package errormsg

// ErrorMsg allows easy setting of common error messages
type ErrorMsg string

// Error returns the ErrorMsg as a string.
// Fullfills the error interface
func (e ErrorMsg) Error() string {
	return string(e)
}

// Very common Error Messages
const (
	ErrorUnknown ErrorMsg = `Unknown error has occurred`
)
