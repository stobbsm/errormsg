# errormsg
Easy library that I end up rewriting a lot. Now it's actually a package, and very simple.

Allows for easy definition of common errors.

### Usage
Install the package using go get
```go
go get gitlab.com/stobbsm/errormsg
```

Import it into your own package
```go
import "gitlab.com/stobbsm/errormsg"
```

### Examples
```go
package myerrors

import "gitlab.com/stobbsm/errormsg"

// Common errors used throughout the package
const (
  UnknownError errormsg.ErrorMsg = `This is an unknown error, common when first developing`
)

func sendError() error {
  return UnknownError
}

func main() {
  if err := sendError(); err != nil {
    log.Error(err)
  }
}
```