package errormsg

import (
	"fmt"
	"strings"
)

// ErrorGroup allows grouping and proccessing of ErrorMsgs
// This is meant to be used to collect errors and then easily
// check for specific ones as needed
type ErrorGroup map[ErrorMsg]int

// NewErrorGroup creates an error group filled with the given errors
func NewErrorGroup(msgs ...error) ErrorGroup {
	e := make(ErrorGroup)
	if len(msgs) > 0 {
		e.Add(msgs...)
	}
	return e
}

// Add appends to the ErrorGroup with the given ErrorMsg's
func (e ErrorGroup) Add(msgs ...error) {
	for _, err := range msgs {
		e[ErrorMsg(err.Error())]++
	}
}

// Error fulfills the error interface
func (e ErrorGroup) Error() string {
	var total int
	var lineFmt = `Error: '%s', Count: %3d`
	var lines = e.lines(lineFmt, func(err error, count int) bool {
		total += count
		return true
	})
	lines = append(lines, fmt.Sprintf(lineFmt, `Total Errors`, total))
	return strings.Join(lines, "\n")
}

// Lines takes a format string and a filter function that returns a boolean
// If the filter returns true for the error message, return the processed line
func (e ErrorGroup) lines(format string, filter func(error, int) bool) []string {
	var lines []string
	for err, n := range e {
		if filter(err, n) {
			lines = append(lines, fmt.Sprintf(format, err, n))
		}
	}
	return lines
}
